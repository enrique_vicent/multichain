basic creation of the images
=====================

build:

```bash
docker build  -t multichain
```

exec:
```bash
docker run -dti --name multichain multichain /bin/bash
docker attach multichain
```

to follow the [get started tutorial](http://www.multichain.com/getting-started/):

run on two termnials `docker run --rm -ti multichain /bin/bash`
